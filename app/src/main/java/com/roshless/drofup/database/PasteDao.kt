package com.roshless.drofup.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface PasteDao {
    @Query("SELECT * FROM pastes")
    fun getAll(): List<Paste>

    @Query("SELECT * FROM pastes WHERE id IN (:ids)")
    fun loadAllByIds(ids: IntArray): List<Paste>

    @Query("SELECT * FROM pastes WHERE id IS :id")
    fun findById(id: Int): Paste

    @Query("SELECT * FROM pastes WHERE name IS :name")
    fun findByName(name: String): Paste

    @Insert
    fun insertAll(vararg pastes: Paste)

    @Delete
    fun delete(paste: Paste)
}


package com.roshless.drofup.network

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.BlobDataPart
import com.github.kittinunf.fuel.core.Method
import com.roshless.drofup.R
import kotlinx.coroutines.*
import java.nio.charset.Charset
import kotlin.coroutines.CoroutineContext

class Requests(context: Context) : CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = mJob + Dispatchers.Main
    private var mJob: Job = Job()

    private val serverURL: String
    private val username: String
    private val password: String
    private val sharedPrefs: SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context)

    init {
        serverURL =
            sharedPrefs.getString(
                context.getString(R.string.sharedprefs_server_url),
                "http://localhost:9090"
            ).toString()
                .plus("/api/")
        username =
            sharedPrefs.getString(context.getString(R.string.sharedprefs_username), "admin")
                .toString()
        password =
            sharedPrefs.getString(context.getString(R.string.sharedprefs_password), "admin")
                .toString()
    }

    fun postFile(input: ByteArray, named: String?): String? {
        val newURL = serverURL.plus("named/?id=").plus(named)
        val (_, response, _) = Fuel.post(newURL)
            .header("username", username)
            .header("password", password)
            .body(input.inputStream().readBytes())
            .responseString()

        if (input.isEmpty() || response.data.isEmpty()) {
            return null
        }
        return response.data.toString(Charset.defaultCharset())
    }

    fun postFile(input: ByteArray): String? {
        val (_, response, _) = Fuel.post(serverURL)
            .header("username", username)
            .header("password", password)
            .body(input.inputStream().readBytes())
            .responseString()

        if (input.isEmpty() || response.data.isEmpty()) {
            return null
        }
        return response.data.toString(Charset.defaultCharset())
    }

    fun deleteFile(name: String): Boolean {
        var retBool: Boolean = false

        runBlocking {
            val deferred = async(Dispatchers.Default) {
                Fuel.delete(serverURL.plus("?id=$name"))
                    .header("username", username)
                    .header("password", password)
                    .responseString(Charset.defaultCharset())
            }
            val response = deferred.await()

            if (response.second.statusCode == 200)
                retBool = true
        }
        return retBool
    }
}